import io
import json
from pathlib import Path

import click

from translate.convert import json2po, pot2po, po2json
from translate.tools import pocount


_HERE = Path(__file__).resolve().parent

CATALOG_FILE = _HERE / 'catalog.pot'

PO_DIR = _HERE / 'po'

JSON_DIR = _HERE / 'json'

LANGS = ['fr', 'en']

SOURCE_LANG = 'en'


def update_catalog(tpl_filename):
    """ Update POT catalog from json file
    generated by schemaBuilder.
    """
    with open(tpl_filename, 'rb') as in_file:
        with open(CATALOG_FILE, 'wb') as out_file:
            json2po.convertjson(
                in_file, out_file,
                template_file=None,
                pot=True,
            )

def update_po(lang):
    """ Update the 'lang' PO file from POT catalog.
    """

    with open(PO_DIR / ('%s.po' % lang), 'rb') as tpl_file:
        tpl_buffer = io.BytesIO(tpl_file.read())

    with open(CATALOG_FILE, 'rb') as in_file:
        with open(PO_DIR / ('%s.po' % lang), 'wb') as out_file:
            pot2po.convertpot(
                in_file, out_file, tpl_buffer,
                fuzzymatching=False,
            )



def po_to_json(tpl_filename, lang):
    """ Generate <lang>.json and <lang>.min.json from
    PO files.
    """
    with open(PO_DIR / ('%s.po' % lang), 'rb') as in_file:
        with open(JSON_DIR / ('%s.json' % lang), 'wb') as out_file:
            with open(tpl_filename, 'rb') as tpl_file:
                res = po2json.convertjson(
                    in_file, out_file, tpl_file,
                    includefuzzy=False,
                    remove_untranslated=False
                )

    with open(JSON_DIR / ('%s.json' % lang), 'r') as json_file:
        loaded = json.load(json_file)

    to_write = {lang: loaded['en']}

    with open(JSON_DIR / ('%s.json' % lang), 'w') as json_file:
        json.dump(to_write, json_file, indent=2)

    with open(JSON_DIR / ('%s.min.json' % lang), 'w') as json_min_file:
        json.dump(to_write, json_min_file)


def to_json(tpl_filename):
    """ Generate json files for all langs and create index file
    """
    index = {}
    for lang in LANGS:
        po_to_json(tpl_filename, lang)
        if lang == SOURCE_LANG:
            # consider SOURCE_LANG as fully translated
            index[lang] = {'pct': 1}
        else:
            # calculate translated counts
            stats = pocount.calcstats(str(PO_DIR / ('%s.po' % lang)))
            pct = stats['translated'] / stats['total'] if stats['total'] else 0
            index[lang] = {'pct': pct}

    with open(JSON_DIR / 'index.json', 'w') as index_file:
        json.dump(index, index_file, indent=2)

    with open(JSON_DIR / 'index.min.json', 'w') as index_min_file:
        json.dump(index, index_min_file)


@click.group()
def cli():
    pass

@cli.command()  # @cli, not @click!
@click.option('-t', '--template-file', required=True, type=click.Path())
def update(template_file):
    update_catalog(template_file)
    for lang in LANGS:
        update_po(lang)

@cli.command()
@click.option('-t', '--template-file', required=True, type=click.Path())
def build(template_file):
    to_json(template_file)


if __name__ == '__main__':
    cli()
