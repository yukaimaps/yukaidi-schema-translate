Yukaidi schema translate
========================

Translations for [Yukaidi 
tagging-schema](https://gitlab.com/yukaimaps/yukaidi-tagging-schema)


Install
-------

Make a python virtualenv and install dependencies

```
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```


Translate
---------

### Get news from tagging schema

Update existing `po/*.po` files from an existing schema build

```
source venv/bin/activate
python translator.py update -t ../yukaidi-tagging-schema/dist/translations/en.json
```

### Translate

Edit each `.po` file which your preferred tools (as Poedit)


### Rebuild translations as json

Make `json/*.json` files from `.po` files

```
source venv/bin/activate
python translator.py build -t ../yukaidi-tagging-schema/dist/translations/en.json
```

